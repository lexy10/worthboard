const express = require("express");
const { auth } = require("../middlewares/auth");
const router = express.Router();

const {
  getAllIncome, addIncome
} = require("../controllers/income");

router.get("/income", auth, getAllIncome);

router.post("/income/add", auth, addIncome);

// with filtering GET /task?completed=true

/*router.get("/services", auth, getAllServices);

router.get("/services/:id", auth, getServicesById);

router.patch("/services/:id", auth, updateServices);

router.delete("/services/:id", auth, deleteServices);*/

module.exports = router;
