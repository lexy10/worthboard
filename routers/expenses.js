const express = require("express");
const { auth } = require("../middlewares/auth");
const router = express.Router();
const {
  addExpenses,
  getAllExpenses,
} = require("../controllers/expenses");

router.get("/expenses", auth, getAllExpenses);

// with filtering GET /task?completed=true

router.post("/expenses/add", auth, addExpenses);

/*router.get("/expenses/:id", auth, getExpensesById);

router.patch("/expenses/:id", auth, updateExpenses);

router.delete("/expenses/:id", auth, deleteExpenses);*/

module.exports = router;
