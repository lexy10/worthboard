const express = require("express");
const { auth } = require("../middlewares/auth");
const router = express.Router();
const { getDashboard, addNewService } = require("../controllers/dashboard");

router.get("/dashboard", auth, getDashboard);
router.post("/dashboard/add", auth, addNewService);

module.exports = router;
