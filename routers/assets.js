const express = require("express");
const { auth } = require("../middlewares/auth");
const router = express.Router();

const {
  addAsset,
  getAllAssets,
} = require("../controllers/assets");

router.get("/assets", auth, getAllAssets);

// with filtering GET /task?completed=true

router.post("/assets/add", auth, addAsset);

// get by id

/*router.get("/assets/:id", auth, getAssetById);

// update assetby id

router.patch("/assets/:id", auth, updateAssets);

router.delete("/assets/:id", auth, deleteAssets);*/

module.exports = router;
