require("dotenv").config();
const express = require("express");
//require("./db/mongoose");
const userRouter = require("./routers/user");
const expenseRouter = require("./routers/expenses");
const serviceRouter = require("./routers/services");
const incomeRouter = require("./routers/income");
const assetRouter = require("./routers/assets");
const AuthRouter = require("./routers/auth");
const dashboardRouter = require("./routers/dashboard");
const db = require("./models/index");

const cors = require("cors");
const morgan = require("morgan");

const app = express();

//___________ routers____________//

app.use(express.json());
app.use(cors());
app.use(morgan("dev"));
app.use("/api", AuthRouter);
app.use("/api", dashboardRouter);
app.use("/api", userRouter);
app.use("/api", expenseRouter);
app.use("/api", serviceRouter);
app.use("/api", incomeRouter);
app.use("/api", assetRouter);

/*db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
});*/

db.sequelize.sync();

module.exports = app;
