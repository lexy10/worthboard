const User = require("../models").users;
const Assets = require('../models').assets;
const Income = require('../models').income;
const Expenses = require('../models').expenses;

exports.getDashboard = async (req, res) => {
  //const _id = req.params.id;
  //console.log(req.user);

  try {
    /*const services = await Services.findOne({ _id, owner: req.user._id });
    if (!services) {
      return res.status(404).send();
    }*/
    const income = await Income.findAll({where: { user_id: req.user.id }, order: [['id', 'DESC']], limit: 2});
    const expenses = await Expenses.findAll({where: { user_id: req.user.id }, order: [['id', 'DESC']], limit: 2});
    const assets = await Assets.findAll({where: { user_id: req.user.id }, order: [['id', 'DESC']], limit: 2});


    res.json({
      status: true,
      user: req.user,
      worth: (Number(req.user.income) + Number(req.user.assets)) - Number(req.user.expenses),
      //incomeMovement: Number(income[0].amount) > Number(income[1].amount) ? 'up' : 'down',
      incomeMovement: income.length > 1 ? (Number(income[0].amount) > Number(income[1].amount) ? 'up' : 'down') : 'up',
      expensesMovement: expenses.length > 1 ? (Number(expenses[0].amount) > Number(expenses[1].amount) ? 'down' : 'up') : 'down',
      assetsMovement: assets.length > 1 ? (Number(assets[0].amount) > Number(assets[1].amount) ? 'up' : 'down') : 'up'
    })
  } catch (error) {
    res.status(500).json({
      status: false,
      message: error.toString()
    });
  }
};

exports.addNewService = async (req, res) => {
  const { category, type, name, amount } = req.body
  try {
    switch (category) {
      case "inc":
        await Income.create({ type: type, name: name, amount: amount, user_id: req.user.id }).then(() => {
          User.update({ income: Number(req.user.income) + Number(amount) }, { where: { id: req.user.id } }).then(()=> {
            res.json({
              status: true,
              message: "New Income added successfully"
            })
          }).catch()
        }).catch();
        break;
      case "exp":
        await Expenses.create({ type: type, name: name, amount: amount, user_id: req.user.id }).then(() => {
          User.update({ expenses: Number(req.user.expenses) + Number(amount) }, { where: { id: req.user.id } }).then(()=> {
            res.json({
              status: true,
              message: "New Expenses added successfully"
            })
          }).catch()
        }).catch();
        break;
      case "ass":
        await Assets.create({ type: type, name: name, amount: amount, user_id: req.user.id }).then(() => {
          User.update({ assets: Number(req.user.assets) + Number(amount) }, { where: { id: req.user.id } }).then(()=> {
            res.json({
              status: true,
              message: "New Asset added successfully"
            })
          }).catch()
        }).catch();
        break;
      default:
        res.json({
          status: false,
          message: "Unable to add new service"
        })
    }
  } catch (e) {
    res.json({
      status: false,
      message: "Unable to add new service"
    })
  }
}
