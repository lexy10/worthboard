const Income = require("../models").income;
const User = require("../models").users;

exports.getAllIncome = async (req, res) => {

  try {
    // first approach
    const income = await Income.findAll({ where: { user_id: req.user.id}});

    res.json(income);
  } catch (error) {
    res.status(500).send();
  }
};

exports.addIncome = async (req, res) =>  {
  const { type, name, amount } = req.body
  await Income.create({
    type: type, name: name, amount: amount, user_id: req.user.id
  }).then(() => {
    User.update({ income: Number(req.user.income) + Number(amount) }, { where: { id: req.user.id } }).then(() => {
      res.json({
        status: true,
        message: "New income added successfully"
      })
    })
  }).catch(() => {
    res.json({
      status: false,
      message: "Unable to add new income"
    })
  })
}

/*exports.getServicesById = async (req, res) => {
  const _id = req.params.id;
  console.log(req.user);

  try {
    const services = await Services.findOne({ _id, owner: req.user._id });
    if (!services) {
      return res.status(404).send();
    }
    res.send(services);
  } catch (error) {
    res.status(500).send();
  }
};

exports.updateServices = async (req, res) => {
  const updates = Object.keys(req.body);

  const allowedUpdates = ["name", "compound", "amount"];

  const isValid = updates.every((update) => allowedUpdates.includes(update));

  if (!isValid) {
    return res.status(400).send({ error: "Invalid Updates" });
  }

  try {
    const services = await Services.findOne({
      _id: req.params.id,
      owner: req.user._id,
    });

    if (!services) {
      return res.status(404).send({ error: "cant find services" });
    }

    updates.forEach((update) => (services[update] = req.body[update]));

    await services.save();
    res.send(services);
  } catch (error) {
    res.status(400).send(error);
  }
};
exports.deleteServices = async (req, res) => {
  try {
    // const task = await Task.findByIdAndDelete(req.params.id);
    const services = await Services.findOneAndDelete({
      _id: req.params.id,
      owner: req.user._id,
    });
    if (!services) {
      res.status(404).send();
    }

    res.send({ message: "services deleted", services });
  } catch (e) {
    res.status(500).send();
  }
};*/
