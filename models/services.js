const servicesSchema = (sequelize, Sequelize) => {
   return sequelize.define('services', {
        name: {
            type: Sequelize.STRING,
            required: true,
            trim: true,
        },
        compound: {
            type: Sequelize.BOOLEAN,
            default: false,
        },
        amount: {
            type: Sequelize.BIGINT,
            default: 0,
        },
    })
}

module.exports = servicesSchema;
