const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    //operatorsAliases: false,
    operatorsAliases: 0,
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true
    },

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const DB = {};

DB.Sequelize = Sequelize;
DB.sequelize = sequelize;

DB.assets = require("./assets")(sequelize, Sequelize);
DB.users = require("./users")(sequelize, Sequelize);
DB.expenses = require("./expenses")(sequelize, Sequelize);
DB.services = require("./services")(sequelize, Sequelize);
DB.income = require("./income")(sequelize, Sequelize);

module.exports = DB;
