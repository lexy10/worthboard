//const mongoose = require("mongoose");

/*const assetSchema = new mongoose.Schema(
  {
    type: {
      type: String,
      default: "assets",
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    compound: {
      type: Boolean,
      default: false,
    },
    amount: {
      type: Number,
      default: 0,
    },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);*/
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("assets", {
        name: {
            type: Sequelize.STRING,
            required: true,
            trim: true,
        },
        type: {
            type: Sequelize.STRING,
        },
        amount: {
            type: Sequelize.BIGINT,
            defaultValue: 0
        },
        user_id: {
            type: Sequelize.INTEGER
        }
    });
};
