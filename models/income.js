const incomeSchema = (sequelize, Sequelize) => {
   return sequelize.define('income', {
       name: {
           type: Sequelize.STRING,
           required: true,
           trim: true,
       },
       type: {
           type: Sequelize.STRING,
       },
       amount: {
           type: Sequelize.BIGINT,
           defaultValue: 0
       },
       user_id: {
           type: Sequelize.INTEGER
       }
    })
}

module.exports = incomeSchema;
