const expensesSchema = (sequelize, Sequelize) => {
    return sequelize.define('expenses', {
        name: {
            type: Sequelize.STRING,
            required: true,
            trim: true,
        },
        type: {
            type: Sequelize.STRING,
        },
        amount: {
            type: Sequelize.BIGINT,
            defaultValue: 0
        },
        user_id: {
            type: Sequelize.INTEGER
        }
    });
}

module.exports = expensesSchema;
